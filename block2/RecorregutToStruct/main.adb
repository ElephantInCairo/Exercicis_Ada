with ada.text_io;         use ada.text_io;
with ada.integer_text_io; use ada.integer_text_io;
with darbre_binari_struc;
with darbre_binari;

procedure main is
	package d_arbre_binari_char is new darbre_binari_struc(character);      use d_arbre_binari_char;
	package d_lector_arbre_char is new darbre_binari(d_arbre_binari_char);  use d_lector_arbre_char;
	a : arbre;
	r : recorregut;
	c : character;
	it : iterador;
	b : boolean;

begin
	--llegim del fitxer
	llegeix_arbre("arbre.txt", a);
	
	--mostram
	new_line;
	put("recorregut inordre: ");
	primer(a, it);
	while es_valid(it) loop
		obt(it, c); seg(a, it);
		put(c);
	end loop;
	
	init_resultats;
	
	--llençam les cridades
	b_inordre("arbre.txt", r); b := arbre_correcte(a, r);
	b := d_lector_arbre_char.es_ABC(a);
	
exception
	when d_lector_arbre_char.entrada_incorrecta => put("l'expressió del recorregut está mal escrita");
	when d_lector_arbre_char.fitxer_no_valid    => put("no s'ha trobat el fitxer o no te el format correcte");
end main;