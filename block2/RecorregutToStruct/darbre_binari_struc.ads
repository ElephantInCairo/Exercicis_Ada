with d_pila;

generic
	type item is (<>);
	 
package darbre_binari_struc is
	type arbre is limited private;
	procedure c_arbre       (a :    out arbre; x : in item);
	procedure posa_dreta    (a : in out arbre; fd : in arbre);
	procedure posa_esquerra (a : in out arbre; fe : in arbre);
	procedure assigna       (a : in arbre; b : out arbre);
	procedure obt_item      (a : in arbre; x : out item);
	procedure obt_dreta     (a : in arbre; x : out arbre);
	procedure obt_esquerra  (a : in arbre; x : out arbre);
	function es_ABC         (a : in arbre) return boolean;
	desbordament       : exception;
	entrada_incorrecta : exception;
	
	type iterador is limited private;
	procedure primer(a : in arbre; it :    out iterador);
	procedure seg   (a : in arbre; it : in out iterador);
	procedure obt     (it : in iterador; x : out item);
	function es_valid (it : in iterador) return boolean;
	mal_us : exception;
	
private
	type node;
	type arbre is access node;
	type node is record
		x      : item;
		fe, fd : arbre;
	end record;
	
	package d_pila_pnode is new d_pila(arbre, assigna); use d_pila_pnode;
	type iterador is record
		p : pila;
	end record;
	
end darbre_binari_struc;

