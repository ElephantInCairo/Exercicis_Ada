with ada.text_io;   
with ada.direct_io;
with d_pila;

package body darbre_binari is
	
	package s_io is new ada.direct_io(character);
	fname : constant string := "../resultats.txt";
	
	--resultat
	procedure init_resultats is
		use s_io;
		f : file_type;
	begin
		open(f, inout_file, fname);
		close(f);
	end init_resultats;
	
	procedure registra_resultat(c : in character; i : in natural) is
		use s_io;
		f : file_type;
		k : positive_count := positive_count(i);
	begin
		open(f, inout_file, fname);
		write(f, c, k);
		close(f);
	end registra_resultat;
	
	--recorregut
	procedure b_preordre(fname : in string; l : out recorregut) is
		use ada.text_io;
		f : file_type;
		a : buffer  renames l.a;
		n : idx     renames l.n;
		c : character;
	begin
		open(f, in_file, fname); skip_line(f);
		n := 0;
		while not end_of_file(f) and then not end_of_line(f) loop
			get(f, c);
			if (c /= ' ') then
				n := n + 1;
				a(n) := c;
			end if;
		end loop;
		close(f);
	exception
		when constraint_error => raise desbordament;
	end b_preordre;
	
	procedure b_inordre(fname : in string; l : out recorregut) is
		use ada.text_io;
		f : file_type;
		a : buffer  renames l.a;
		n : idx      renames l.n;
		c : character;
	begin
		open(f, in_file, fname);
		n := 0;
		while not end_of_file(f) and then not end_of_line(f) loop
			get(f, c);
			if (c /= ' ') then
				n := n + 1;
				a(n) := c;
			end if;
		end loop;
		close(f);
	exception
		when constraint_error => raise desbordament;
	end b_inordre;
	
	--r_iterador
	procedure primer (it : out r_iterador) is
	begin
		it := 1;
	end primer;
	
	procedure seg (it : in out r_iterador) is
	begin
		if it = max then raise mal_us; end if;
		it := it + 1;
	end seg;
	
	procedure obt (l : in recorregut; it : in r_iterador; c : out character) is
		a : buffer  renames l.a;
		n : idx renames l.n;
		i : idx := idx(it);
	begin
		if i > n then raise mal_us; end if;
		c := a(i);
	end obt;
	
	function es_valid (l : in recorregut; it : in r_iterador) return boolean is
		i : idx := idx(it);
		n : idx renames l.n;
	begin
		return i <= n;
	end es_valid;
	
	--part pública
	procedure llegeix_arbre (fname : in string; a : out arbre) is
		package d_pila_arbre is new d_pila(arbre, assigna); use d_pila_arbre;
		lpre, lin      : recorregut;
		cent, inordre_trobat, a0, pare0 : arbre;
		no_acabats     : pila;
		c : character;
		
		procedure llegeix_arbre(a : in out arbre) is
			p, i : character;
			itpre, itin : r_iterador;
		begin
			primer(itpre); primer(itin);
			while es_valid(lpre, itpre) loop
				--afegeix el node a la dreta del darrer inordre_trobat
				obt(lpre, itpre, p); seg(itpre);
				c_arbre(a0, p); posa_dreta(inordre_trobat, a0);
				empila(no_acabats, a0);
				obt(lin, itin, i);
				while es_valid(lpre, itpre) and p /= i loop
					--afegeix els nodes a l'esquerra
					obt(lpre, itpre, p); seg(itpre);
					assigna(a0, pare0);
					c_arbre(a0, p); posa_esquerra(pare0, a0);
					empila(no_acabats, a0);
				end loop;
				while es_valid(lin, itin) and p = i loop
					--ajusta els nodes que ja hem processat la branca esquerra
					assigna(a0, inordre_trobat); desempila(no_acabats); 
					cim(no_acabats, a0); obt_item(a0, p);
					seg(itin); if es_valid(lin, itin) then obt(lin, itin, i); end if;
				end loop;
			end loop;
			obt_dreta(cent, a);
		end llegeix_arbre;
		
	begin
		--init: pre-arrel, pila, lectors
		c_arbre(cent, '$'); assigna(cent, inordre_trobat);
		buida(no_acabats); empila(no_acabats, cent);
		b_preordre(fname, lpre); b_inordre(fname, lin);
		--algorisme
		llegeix_arbre(a);
		--verificam que es correcte
		cim(no_acabats, a0); obt_item(a0, c);
		if c /= '$' then raise entrada_incorrecta; end if;
	exception
		when d_pila_arbre.mal_us => raise entrada_incorrecta;
	end llegeix_arbre;
	
	function es_ABC (a : in arbre) return boolean is
		c : character;
		es_ABC : boolean;
	begin
		es_ABC := darbre_char.es_ABC(a);
		--output a fitxer
		if es_ABC then c := '1'; else c := '0'; end if;
		registra_resultat(c, 2);
		return es_ABC;
	end es_ABC;
	
	function arbre_correcte (a : in arbre; r : in recorregut) return boolean is
		use ada.text_io;
		i  : iterador;
		ri : r_iterador;
		c, d : character;
		arbre_correcte : boolean;
	begin
		arbre_correcte := true;
		primer(a, i); primer(ri);
		while es_valid(i) and es_valid(r, ri) and arbre_correcte loop
			obt(i, c);     seg(a, i);
			obt(r, ri, d); seg(ri);
			arbre_correcte := c = d;
		end loop;
		arbre_correcte := arbre_correcte and not (es_valid(i) or es_valid(r, ri));
		--output a fitxer
		if arbre_correcte then c := '1'; else c := '0'; end if;
		registra_resultat(c, 1);
		return arbre_correcte;
	end arbre_correcte;
	
end darbre_binari;
