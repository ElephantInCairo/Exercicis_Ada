with darbre_binari_struc;

generic
	with package darbre_char is new darbre_binari_struc(character);
	mida : natural := 50;

package darbre_binari is
	--lector
	type recorregut is limited private;
	procedure b_preordre(fname : in string; l : out recorregut);
	procedure b_inordre (fname : in string; l : out recorregut);
	fitxer_no_valid : exception;
	
	--iterador des lector
	type r_iterador is private;
	procedure primer  (it :    out r_iterador);
	procedure seg     (it : in out r_iterador);
	procedure obt     (l  : in recorregut; it : in r_iterador; c : out character);
	function es_valid (l  : in recorregut; it : in r_iterador) return boolean;
	mal_us : exception;
	
	--operacions d'abres amb i/o a fitxers
	use darbre_char;
	procedure llegeix_arbre (fname : in string; a : out arbre);
	function es_ABC         (a : in arbre)                    return boolean;
	function arbre_correcte (a : in arbre; r : in recorregut) return boolean;
	entrada_incorrecta : exception;
	
	--escritor
	procedure init_resultats;
	
private
	type idx is new natural range 0 .. mida;
	type buffer is array(idx) of character;
	type recorregut is record
		a : buffer;
		n : idx;
	end record;
	
	type r_iterador is new idx;
	max : r_iterador := r_iterador(mida);

end darbre_binari;
