package body darbre_binari_struc is

	--iteradors
	procedure empila_cami_esq(a : in arbre; p : in out pila) is
		a0 : arbre := a;
	begin
		while a0 /= null loop
			empila(p, a0);
			a0 := a0.fe;
		end loop;
	end empila_cami_esq;
	
	procedure primer(a : in arbre; it : out iterador) is
		p : pila renames it.p;
	begin
		buida(p); empila_cami_esq(a, p);
	end primer;
	
	function es_valid(it : iterador) return boolean is
		p : pila renames it.p;
	begin
		return not es_buida(p);
	end es_valid;
	
	procedure obt(it : in iterador; x : out item) is
		p : pila renames it.p;
		a0 : arbre;
	begin
		if es_buida(p) then raise mal_us; end if;
		cim(p, a0); x := a0.x;
	end obt;
	
	procedure seg(a : in arbre; it : in out iterador) is
		p : pila renames it.p;
		a0 : arbre;
	begin
		if es_buida(p) then raise mal_us; end if;
		cim(p, a0); desempila(p); 
		if a0.fd /= null then empila_cami_esq(a0.fd, p); end if;
	end seg;
	
	--arbres	
	procedure c_arbre (a : out arbre; x : in item) is
	begin
		a := new node'(x, null, null);
	exception
		when storage_error => raise desbordament;
	end c_arbre;
	
	procedure posa_dreta (a : in out arbre; fd : in arbre) is
	begin
		a.fd := fd;
	end posa_dreta;
	
	procedure posa_esquerra (a : in out arbre; fe : in arbre) is
	begin
		a.fe := fe;
	end posa_esquerra;
	
	procedure assigna (a : in arbre; b : out arbre) is
	begin
		b := a;
	end assigna;
	
	procedure obt_item (a : in arbre; x : out item) is
	begin
		x := a.x;
	end obt_item;
	
	procedure obt_dreta (a : in arbre; x : out arbre) is
	begin
		x := a.fd;
	end obt_dreta;
	
	procedure obt_esquerra (a : in arbre; x : out arbre) is
	begin
		x := a.fe;
	end obt_esquerra;
	
	function es_ABC(a : in arbre) return boolean is
		it   : iterador;
		i, j : item;
		es_ABC : boolean;
	begin
		es_ABC := true;
		primer(a, it);
		if es_valid(it) then obt(it, i); seg(a, it); end if;
		while es_valid(it) and es_ABC loop
			obt(it, j); seg(a, it);
			es_ABC := i < j;
			i := j;
		end loop;
		return es_ABC;
	end es_ABC;
	
	
end darbre_binari_struc;