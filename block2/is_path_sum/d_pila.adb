package body d_pila is

    procedure buida (s: out pila) is
        top : index renames s.top;
    begin
        top := 0;
    end buida;

    procedure empila (s: in out pila; x: in item) is
        top : index     renames s.top;
        m   : mem_space renames s.m;
    begin
        if top = index(max) then raise space_overflow; end if;
        top := top + 1; m(top) := x;
    end empila;

    function cim (s: in pila) return item is
        top : index     renames s.top;
        m   : mem_space renames s.m;
    begin
        if top = 0 then raise bad_use; end if;
        return m(top);
    end cim;

    procedure desempila (s: in out pila) is
        top : index renames s.top;
    begin
        if top = 0 then raise bad_use; end if;
        top := top - 1;
    end desempila;

    function es_buida (s:in pila) return boolean is
        top : index renames s.top;
    begin
        return top = 0;
    end es_buida;

end d_pila;
