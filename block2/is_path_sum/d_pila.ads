generic
    type item  is private;
    max : natural := 50;

package d_pila is
    pragma pure;
    type pila is limited private;
    bad_use        : exception;
    space_overflow : exception;
    procedure buida     (s :    out pila);
    procedure desempila (s : in out pila);
    procedure empila    (s : in out pila; x: in item);
    function cim        (s : in     pila) return item;
    function es_buida   (s : in     pila) return boolean;

private
    type index is new natural range 0 .. max;
    type mem_space is array (index range 1 .. index(max)) of item;
    type pila is record
        m   : mem_space;
        top : index;
    end record;

end d_pila;
