with d_pila;

package body d_binari_1 is
	
	--definim iteradors
	type block is record 
		a : pnode;
		x : item; --estil mapping
	end record;
	package d_iterador is new d_pila(block, 50); use d_iterador;
	subtype iterador   is pila;
	
	--omplim l'iterador amb blocks de tal forma que:
	-- 1. b.a : node de l'abre d'acord al recorregut inordre
	-- 2. b.x : suma parcial desde l'arrel al pnode a (aquest inclòs)
	procedure empila_br_esq (a : in pnode; it : in out iterador; base : in item) is
		sa    : pnode := a;
		base0 : item  := base;
	begin
		while sa /= null loop
			base0 := base0 + sa.x;
			empila(it, (sa, base0));
			sa := sa.fe;
		end loop;
	end empila_br_esq;
	
	--empilam el camí 'esquerre'
	procedure primer (a : in arbre; it : out iterador) is
		a0 : pnode renames a.a;
	begin
		buida(it); empila_br_esq(a0, it, first_item);
	end primer;
	
	--desempilam la fulla visitada i cercam la seg
	procedure seg (it : in out iterador) is
		b : block;
	begin
		desempila(it);
		if not es_buida(it) then
			b := cim(it); desempila(it);
			empila_br_esq(b.a.fd, it, b.x);
		end if;
	end seg;
	
	--obtenim la suma de la fulla
	procedure obt(it : in iterador; x : out item) is
		b : block;
	begin
		b := cim(it); x := b.x;
	end obt;
	
	--al fer seg a la darrera fulla es queda buida, com inordre
	function es_valid(it : in iterador) return boolean is
	begin
		return not es_buida(it);
	end es_valid;
	
	
	--part pública	  
	procedure inserir(a: in out arbre; x: in item) is
		ne : natural renames a.ne;
		n, p : pnode;
		
		procedure obt_pare(a : in pnode; i : in natural; a0 : out pnode) is
			prof_pare, prof : natural;
			q, dq : natural;
			
			function prof0 (i : in natural) return natural is
				i0 : natural := i;
				x : natural;
			begin
				x := 0;
				while i0 > 0 loop
					x := x + 1;
					i0 := i0 / 2;
				end loop;
				return x;
			end prof0;
			
		begin
			prof := 1; prof_pare := prof0(i) - 1;
			dq := 2 ** prof_pare;
			q := ne - dq; --descartam el primer bit (arrel)
			a0 := a;
			while prof < prof_pare loop
				dq := dq / 2;
				if q / dq = 0 then
					a0 := a0.fe;
				else
					q := q - dq;
					a0 := a0.fd;
				end if;
				prof := prof + 1;
			end loop;
		end obt_pare;
		
	begin
		n := new node'(x, null, null); --obtenim mem.
		ne := ne + 1;
		if ne = 1 then
			a.a := n; --cas arrel
		else
			obt_pare(a.a, ne, p);
			if ne mod 2 = 0 then
				p.fe := n; --cas fill dret de no arrel
			else
				p.fd := n;  --cas fill esq de no arrel
			end if;
		end if;
	--exception
	--	when storage_error => raise desbordament;
	end inserir;
	
	function is_path_sum(a: in arbre; x: in item) return boolean is
		it : iterador;
		b  : block;
	begin
		primer(a, it);
		while es_valid(it) loop
			b := cim(it); seg(it);
			if b.x = x then return true; end if;
		end loop;
		return false;
	end is_path_sum;
	
end d_binari_1;