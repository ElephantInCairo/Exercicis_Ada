
generic
	type item is private;
	first_item: item; -- element neutre del tipus item, si son nombres sera 0
	with function "+"(a,b: in item) return item;

package d_binari_1 is
	type arbre is limited private;
	-- no pot haver mes procediments o funcions publiques
	procedure inserir(a: in out arbre; x: in item);
	function is_path_sum(a: in arbre; x: in item) return boolean;

private
	type node;
	type pnode is access node;
	type node is record
		x : item;
		fd, fe : pnode;
	end record;
	type arbre is record
		a : pnode := null;
		ne : natural := 0;
	end record;

end d_binari_1;