with ada.integer_text_io; use ada.integer_text_io;

package body d_lector is

	procedure b_fitxer_enters (fnom : in string; fe : out fitxer_enters) is
		f : file_type renames fe.f;
	begin
		open(f, in_file, fnom); seg(fe);
	exception
		when name_error => raise fitxer_no_trobat;
	end b_fitxer_enters;
	
	procedure seg (fe : in out fitxer_enters) is
		f   : file_type renames fe.f;
		ev  : boolean   renames fe.ev;
		act : integer   renames fe.act;
	begin
		if not end_of_file(f) then
			ev := true; get(f, act);
		else
			ev := false;
		end if;
	end seg;
	
	function obt_enter(fe : in fitxer_enters) return integer is
		act : integer renames fe.act;
	begin
		return act;
	end obt_enter;
	
	function es_valid (fe : in fitxer_enters) return boolean is
		ev : boolean renames fe.ev;
	begin
		return ev;
	end es_valid;

end d_lector;
