with ada.text_io; use ada.text_io;

package d_lector is
	type fitxer_enters is limited private;
	procedure b_fitxer_enters(fnom : string; fe : out fitxer_enters);
	procedure seg     (fe : in out fitxer_enters);
	function obt_enter(fe : in fitxer_enters) return integer;
	function es_valid (fe : in fitxer_enters) return boolean;
	mal_us           : exception;
	fitxer_no_trobat : exception;
	
private
	type fitxer_enters is record
		f   : file_type;
		ev  : boolean;
		act : integer;
	end record;
	
end d_lector;
