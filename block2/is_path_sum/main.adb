with ada.command_line; use ada.command_line;
with ada.text_io; use ada.text_io;
with ada.direct_io;
with ada.integer_text_io; use ada.integer_text_io;
with d_lector; use d_lector;
with d_binari_1;


procedure main is
	-- def basiques
	package d_binari_1_enters is new d_binari_1(integer, 0, "+"); use d_binari_1_enters;
	nombre_darguments : exception;
	
	procedure put_file(c : in character) is
		package char_io is new ada.direct_io(character); use char_io;
		f : char_io.file_type;
	begin
		open(f, inout_file, "../resultats.txt");
		write(f, c, 3);
		close(f);
	end put_file;
	
	--vars
	l : fitxer_enters;
	a : arbre;
	suma, ent : integer;

begin
	--precondicions:
	if argument_count /= 2 then raise nombre_darguments; end if;
	
	--posam noms i tipus
	suma := integer'value(argument(2));
	
	--llegim del fitxer
	b_fitxer_enters(argument(1), l); --buida(a);
	while es_valid(l) loop
		ent := obt_enter(l); seg(l);
		inserir(a, ent);
	end loop;

	--operam i mostram
	if is_path_sum(a, suma) then put_file('1');
	else                         put_file('0');
	end if;
	
exception
	when nombre_darguments => put("main.exe nombre_fitxer suma_total");
	when fitxer_no_trobat  => put("el fitxer "& argument(1) &" no existeix");
	
end main;